---
title: "Impuestos"
weight: 2
resources:
    - src: plant.jpg
      params:
          weight: -100
---

## Impuestos

##### QUE VUELVAN 
Es imprescindible que los impuestos vuelvan en mejores servicios públicos. 

Que puedas atenderte en un hospital cerca de tu casa, que te llegue el asfalto, que no tengas que esperar semanas por un trámite.  

Es fundamental alentar la reinversión del dinero público en la sociedad de manera inteligente, con planificación en una misiones del futuro.

##### ESTADO INTELIGENTE

Cuando existan años en los que el Estado recaude más, establecer un porcentaje fijo de esa recaudación impositiva a obras de infraestructura, insumos o salarios de los trabajadores del estado (personal de salud, docentes, etc.).

Esta es una medida que busca disminuir la discrecionalidad del gasto político y busca reivindicar a los verdaderos protagonistas, los docentes, el personal de salud y demás trabajadores a los que la plata no les alcanza.