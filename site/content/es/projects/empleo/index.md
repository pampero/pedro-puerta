---
title: "Empleo"
weight: 2
---

## MISIONES ES TRABAJO

En un gobierno nacional que no tiene rumbo ni plan económico, hoy Misiones sufre las consecuencias de la improvisación y es víctima de asimetrías regionales muy profundas.

Esta propuesta la diseño un equipo de profesionales de diferentes áreas y apunta a soluciones concretas, enfocadas mayormente en el corto y mediano plazo.

Trabajar para vivir y no para sobrevivir. 

La esperanza de que el empleo sea un derecho de los misioneros, con más empleo privado y con mejores sueldos en el empleo público. 

Atraer más y mejores inversiones con una Misiones abierta al mundo, aprovechando nuestras ventajas comparativas de frontera

##### EMPLEO JOVEN:

Un cupo fijo de pasantías pagas en trabajos a distancia para jóvenes misioneros en empresas nacionales e internacionales con las que ya hicimos contacto (IBM, Google, Amazon, Facebook, etc.) Una idea que busca que los chicos puedan sumar experiencia sin moverse de sus casas y saltear las barreras impositivas que tienen las empresas para venir a radicarse en Misiones.

##### EMPLEO PRIVADO:

Hoy en día cada empleado cuesta un 40% más por aportes patronales y demás impuestos que se cargan sobre las empresas. Disminuir las cargas sociales ayudaría a que estos ingresos puedan volcarse a contratar nuevo personal y créditos de consumo para cada uno de los empleados. Hay que sacar el dinero de las arcas del estado para inyectarlo directamente en el bolsillo del misionero y de las empresas que producen.

Tenemos propuestas, tenemos la vocación de transformar nuestra provincia.